import hid

# Implements some helpers dor getting usb information

def list_usb_devices():
	print("\x1b[01m   vend prod interface usage usage_page path\x1b[00m")
	for desc in hid.enumerate():
		highlight="   "
		if desc["vendor_id"] == 0x04d9 and desc["product_id"] == 0xa116:
			highlight="\x1b[32m * "
			if desc["interface_number"] == 2:
				highlight="\x1b[01;32m-> "
		if desc["vendor_id"] == 0x04d9 and desc["product_id"] == 0x8022:
			highlight="\x1b[35m * "
		print(f'{highlight}{desc["vendor_id"]:04x}:{desc["product_id"]:04x} {desc["interface_number"]:<9d} {desc["usage"]:<5d} {desc["usage_page"]:<10d} {desc["path"].decode("ascii")}\x1b[00m')

list_usb_devices()
