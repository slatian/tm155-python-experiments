#requires python hidapi

import hid
import time

COMMAND_FIRMWARE_VERSION = 0
COMMAND_FLAGS = 1
COMMAND_PROFILE = 2
COMMAND_REPORT_RATE = 3
COMMAND_DPI_STAGE = 4
COMMAND_SIDE_BUTTON_LEDS = 5
COMMAND_CLEAR_REPORTS = 8
COMMAND_BLINK_LIGHTS = 9
COMMAND_BOOTLOADER = 0xa

BULK_COMMAND_CONFIG = 0xC
BULK_COMMAND_BUTTON_MAP = 0xD
BULK_COMMAND_MACRO = 0xF

def connect_to_mouse():
	for desc in hid.enumerate():
		# search for a holtek device that makes the tm-115 tick
		if desc["vendor_id"] == 0x04d9 and desc["product_id"] == 0xa116:
			# test if we have the right interface
			if desc["interface_number"] == 2:
				# and is the usage the proprietary crap
				# for some reason this doesn't work for me currently
				if True: #desc["usage"] == 0xff00 and desc["usage_page"] == 0xff00:
					device = hid.device()
					device.open_path(desc["path"])
					return device

def open_bricked_mouse():
	for desc in hid.enumerate():
		# search for a holtek device that makes the tm-115 tick
		if desc["vendor_id"] == 0x04d9 and desc["product_id"] == 0x8022:
			# test if we have the right interface
			if desc["interface_number"] == 0:
				device = hid.device()
				device.open_path(desc["path"])
				return device

class TM155Mouse:
	def __init__(self, device):
		self.device = device

	# raw device access
	
	def send_command(self, command:int, arg1:int=0, arg2:int=0, arg3:int=0, arg4:int=0, arg5:int=0, arg6:int=0):
		self.device.send_feature_report([command,arg1,arg2,arg3,arg4,arg5,arg6,0xFF-((command+arg1+arg2+arg3+arg4+arg5+arg6)&0xFF)])

	def send_read_command(self, command:int, arg1:int=0, arg2:int=0):
		self.send_command(command|0x80, arg1, arg2)
		return self.device.get_feature_report(0,9)

	# wrapped
	def get_firmware_version(self):
		res = self.send_read_command(COMMAND_FIRMWARE_VERSION)
		return [res[2],res[3]]

	def set_flags(self, flags: int):
		check_byte()
		self.send_command(COMMAND_FLAGS, flags)

	def get_flags(self) -> int:
		res = self.send_read_command(COMMAND_FLAGS)
		return res[2]

	def set_profile(self, profile_id: int):
		check_profile_id(profile_id)
		self.send_command(COMMAND_PROFILE, profile_id)

	def get_profile(self) -> int:
		res = self.send_read_command(COMMAND_PROFILE)
		return res[2]

	def set_report_rate(self, profile_id: int, report_rate: int):
		check_profile_id(profile_id)
		self.send_command(COMMAND_REPORT_RATE, profile_id, report_rate)

	def get_report_rate(self, profile_id: int) -> int:
		check_profile_id(profile_id)
		res = self.send_read_command(COMMAND_REPORT_RATE, profile_id)
		return res[3]

	def set_dpi_stage(self, profile_id: int, dpi_stage: int):
		check_profile_id(profile_id)
		# TODO:
		# 4 is iirc not the maximum,
		# which depends on the current configuration
		check_byte(dpi_stage, "DPI Stage", min=1, max=4)
		self.send_command(COMMAND_DPI_STAGE, profile_id, dpi_stage)

	def get_dpi_stage(self, profile_id: int) -> int:
		check_profile_id(profile_id)
		res = self.send_read_command(COMMAND_DPI_STAGE, profile_id)
		return res[3]

	def set_side_led(self, on: bool):
		self.send_command(COMMAND_SIDE_BUTTON_LEDS, int(on))

	def get_side_led(self) -> bool:
		res = self.send_read_command(COMMAND_SIDE_BUTTON_LEDS)
		return bool(res[2])

	def clear_reports(self):
		self.send_command(COMMAND_CLEAR_REPORTS, 0xaa, 0xcc, 0xee)

	# This one is evil 😈
	#def activate_bootloader(self):
	#	self.send_command(COMMAND_BOOTLOADER, 0xaa, 0x55, 0xcc, 0x33, 0xbb, 0x99)

	def check_bootloader(self):
		return self.send_read_command(COMMAND_BOOTLOADER)[2] == 0xff;

	def blink(self, r, g, b, delay, count):
		check_byte(r)
		check_byte(g)
		check_byte(b)
		check_byte(count)
		check_byte(delay)
		self.send_command(COMMAND_BLINK_LIGHTS, r, g, b, delay, count)


#type checking
def check_profile_id(profile_id:int):
	# TODO: figure out how many profiles this thing actually has
	check_byte(profile_id, "Profile id", max=16)
	
def check_byte(value:int, name:str="Byte", min=0, max=255):
	if value > max or value < min:
		raise ValueError(name+" is out of bounds!")
	
# Legacy functions

def send_bulk_read_command(device, command, arg=0):
	command = command | 0x80
	device.send_feature_report([command,arg,0,0,0,0,0,0xFF-((command+arg)&0xFF)])
	print(device.get_feature_report(0, 9))
	print(device.read(0x40))
	print(device.read(0x40))
	
def send_bulk_write_command(device, command, arg=0, data1 = b'' , data2 = b''):
	l = len(data1)+len(data2)
	print(device.send_feature_report([command,arg,l,0,0,0,0,0xFF-((command+arg+l)&0xFF)]))
	print(device.write(data1))
	time.sleep(0.05)
	print(device.write(data2))
	
def send_command(device, command, arg1=0, arg2=0):
	device.send_feature_report([command,arg1,arg2,0,0,0,0,0xFF-((command+arg1+arg2)&0xFF)])

def send_read_command(device, command, arg1=0, arg2=0):
	send_command(device, command|0x80, arg1, arg2)
	return device.get_feature_report(0,9)
