import tm155

device = tm155.connect_to_mouse()

profile_id = 2

print("Old Block D config:")
tm155.send_bulk_read_command(device, 0xd, profile_id)
print("")

new_d_block_config = [
	1, 0, 240, 0, # left mouse button
	1, 0, 241, 0, # right mouse button
	1, 0, 242, 0, # middle mouse buttons
	7, 0, 1,   0, # dpi stage up
	7, 0, 2,   0, # dpi stage down
	1, 0, 245, 0, # tilt left
	1, 0, 246, 0, # tilt right
	0,15, 0x3E,0, # Press button S5
	0,15, 0x3D,0, # Press button S4
	0,15, 0x3C,0, # Press button S3
	0,15, 0x3B,0, # Press button S2
	0, 4, 0,   0, # Press button S1
	0, 0, 0,   0, # nop
	0, 0, 0,   0, # nop
	4, 0, 1,   0, # scroll up
	4, 0, 2,   0, # scroll down
]

def send_bulk_write_command_short(device, command, arg=0, data1 = b'', profile_id=0):
	l = len(data1)
	print(device.send_feature_report([command,arg,l,0,0,0,0,0xFF-((command+arg+l)&0xFF)]))
	print(device.write(data1))

send_bulk_write_command_short(device, 0xd, profile_id, new_d_block_config)
#tm155.send_bulk_write_command(device, 0xd, profile_id, new_d_block_config, new_d_block_config)
print("")

print("New Block D config:")
tm155.send_bulk_read_command(device, 0xd, profile_id)
print("")
