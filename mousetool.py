import tm155
import sys
import argparse

argparser = argparse.ArgumentParser(
	prog="mousetool-tm155",
	description="A hacky tool to control TM-155 Mice sold under various brands"
)

argparser.add_argument("command")
argparser.add_argument("--set")
argparser.add_argument("--red",type=int,default=0xff)
argparser.add_argument("--green",type=int,default=0xff)
argparser.add_argument("--blue",type=int,default=0xff)
argparser.add_argument("--count", "-n",type=int,default=3)
argparser.add_argument("--delay",type=int,default=16)
argparser.add_argument("--profile", "-p", type=int)
#argparser.add_argument("--activate-bootloader", action='store_true')

args = argparser.parse_args()

device = tm155.connect_to_mouse()

if device is None:
	print("Can't connect to mouse!")
	sys.exit(1)

mouse = tm155.TM155Mouse(device)

def get_profile():
	if args.profile is None:
		return mouse.get_profile()
	return args.profile

def to_bool(s):
	match s:
		case ["0" | "off" | "no" | "false"]:
			return False
		case ["1" | "on" | "yes" | "true"]:
			return True
	return bool(s)

try:
	match args.command:
		case "firmware":
			print(mouse.get_firmware_version())
		case "profile":
			if args.set is not None:
				mouse.set_profile(int(args.set))
			print(mouse.get_profile())
		case "flags":
			print(mouse.get_flags())
		case "report-rate":
			profile = get_profile()
			print(mouse.get_report_rate(profile))
		case "dpi-stage":
			profile = get_profile()
			if args.set is not None:
				mouse.set_dpi_stage(profile, int(args.set))
			print(mouse.get_dpi_stage(profile))
		case "side-led":
			try:
				if args.set is not None:
					mouse.set_side_led(to_bool(args.set))
				print(mouse.get_side_led())
			except OSError:
				print("The led command is not supported on your Mouse.")
				sys.exit(4)
		#case "bootloader":
			#if args.activate_bootloader:
			#	mouse.activate_bootloader()
			#print(mouse.check_bootloader())
		case "blink":
			mouse.blink(args.red, args.green, args.blue, args.delay, args.count)
			
			
			
except ValueError as e:
	print("The command you tried to invoke threw an error:")
	print(e)
	sys.exit(3)
