import tm155

device = tm155.connect_to_mouse()

profile_id = 1

print("Block C config:")
tm155.send_bulk_read_command(device, 0xc, profile_id)
print("")

print("Block D config:")
tm155.send_bulk_read_command(device, 0xd, profile_id)
print("")
