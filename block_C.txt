[
	142, 0  , 63 , 0,
	51 , 170, 22 , 0,
	0  , 0  , 0  , 0, # key, do NOT touch
	0  , 0  , 0  , 0, # key, do NOT touch
	8  , 4  , 2  , 1, # dpi stage leds
	12 , 14 , 15 , 7, # dpi stage leds
	255, 0  , 0  , # color: rgb cycle 0   red
	0  , 255, 0  , # color: rgb cycle 1   green
	0  , 0  , 255, # color: rgb cycle 2   blue
	255, 0  , 255, # color: rgb cycle 3   pink
	0  , 255, 255, # color: rgb cycle 4   cyan
	255, 0  , 255, # color: rgb cycle 5 ? pink
	160, 160, 160, # grey
	255, 255, 255, # white
	30 , 0  , 0  , 0,
	0  , 0  , 0  , 0,
	0  , 0  , 0  , 0,
	0  , 0  , 0  , 0,
]
[
	15 , 4  , 10 , 10,
	25 , 25 , 7  , 3,
	24 , 1  , 100, 100,
	1  , 192, 240, 3,
	1  , 1  , 100, 0,
	12 , 24 , 48 , 72,
	108, 144, 108, 56,
	0  , 0  , 0  , 0,
	0  , 0  , 1  , 0,
	127, 255, 255, 0,
	255, 0  , 0  , 0,
	0  , 255, 0  , 255,
	0  , 255, 255, 0,
	0  , 255, 255, 255, 
	128, 0  , 255, 0,
	160, 255, 255, 255
]

layout:
0x02: bitfield: enabled profiles

0x10: bitfield: dpi leds 0
0x11: bitfield: dpi leds 1
0x12: bitfield: dpi leds 2
0x13: bitfield: dpi leds 3
0x14: bitfield: dpi leds 4
0x15: bitfield: dpi leds 5
0x16: bitfield: dpi leds 6
0x17: bitfield: dpi leds 7
0x18: color: rgb cycle 0
0x1b: color: rgb cycle 1
0x1e: color: rgb cycle 2
0x21: color: rgb cycle 3
0x24: color: rgb cycle 4

0x40:
	enable1000Hz -> 1
	enable500Hz  -> 2
	enable250Hz  -> 4
	enable125Hz  -> 8
0x46: dpi stages ?
0x47: light modi
	0: off
	1: constant
	2: breathing
	3: neon
0x48: light change delay
0x49: light color source
	0: white light, no idea from where
	1: white light, no idea from where
	2: red light
	3: dpi stage color
	
0x68: color: dpi stage 0
0x6b: color: rgb cycle 1
0x6e: color: rgb cycle 2
0x71: color: rgb cycle 3

colors:
	R G B
