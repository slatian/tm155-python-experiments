# TM155 experiments

This repository contains information on what bytes to poke where to make TM155 mice do what you want.

Most information here is thaken from Ninji's blog post series over at [https://wuffs.org/blog/mouse-adventures](https://wuffs.org/blog/mouse-adventures).
The rest was found out through trial and error.

## Possibly compatible Mice

If your mouse indentifies itself with `04d9:a116 Holtek Semiconductor, Inc. USB Gaming Mouse`, chances are the information here applys to it.

- Titanwolf Specialist (The one I have and tested on)
- Tecknet Hypertrak (The one Ninji has/had in 2018)
More: https://wuffs.org/blog/mouse-adventures-part-2

(Yes this an attempt to do a bit of seo to make this easier to find)

## The Python Utilitys

The [tm155.py](tm155.py) file contains some helper functions for poking around in the mouse, they are based on Ninji's examples and use the `hidapi` python library. The test*.py files are for experimentation purposes, try to understand roughly what they do befor running them (you probably want to modify them too).

You can dump the configuration of your mouse using the dump_config.py script, make sure you keep a copy of the default configuration around.

NOTE: All scripts have a profile_id variable that allows you to choose a mose profile that will be written to.
The default will usually be profile 2 because that's what I use for testing. The test_set_profile.py script can be used to set the currently active profile. There are 6 profiles according to the manual that came with my mouse (starting at 0)

## Magic Values

You can find a lot of magic values in Ninji's configuration utility: https://github.com/Treeki/TM155-tools/blob/master/tm155-mac/tm155-tool-x/TM155ControlDevice.swift

## Bootloader Mode

Do not enable bootloader mode by hand if you aren't prepared to disassemble your mouse and flash fresh firmware using it's holtek ISP interface.

In case you did anyway: DO NOT UNPLUG IT, if it looses power, it will probably become bricked. You should now see it with a slightly different configuration accepting 8 byte feature reports.

Holteks ISPDLL.dll reveals some reports that get sent by the update application (absolutely no guarantees given, because guess how I found out that unplugging bricks your mouse):

In `ConnectToBootloader`:
* send `[0,0,0,0, 0,0,0,0]`, try to read a response by reading version information.
* send `[7,0,0,0, 0,0,0,0]`
* send `[5,x,0,0, 0,0,0,0]` where x is 0 or 1 depending on some setting, sent based on wat is returned by the bootloader version.

In `GetBootloaderVer`:
* send `[0,0,0,0, 0,0,0,0]`
* store response to global variable

In `SwitchToUserProgram`:
* send `[8,0,0,0, 0,0,0,0]`

but to repeat: I tested none of these, my old tm-155 is bricked and I'm not risking my new one. Do not switch your mouse too bootloader mode unless you have a backup of it's firmware and you are familiar with working on holtek controllers.

I case you want to find the files I worked with (really big THANK YOU to Ninji):

|        | ISPDLL.dll                                                       | HIDDLL.dll                                                       |
|-------:|:-----------------------------------------------------------------|:-----------------------------------------------------------------|
| md5    | 016304cf2619f6d54d4e8d3b05475547                                 | f29097864571907ee91b2ce0c9b1be13                                 |
| sha1   | badbe850acb01d0dcfdf01733cf9349ac491e722                         | ff52a8b52b26808385836d1f3e25c68bf75c5a05                         |
| sha256 | 532993f0f33056e49dd518f0c672cf5a7307f9a524cd0a7c9786db65b4c896cd | 3b574c681b64a28786d4f95802081f86d9f925262c4e6ab61ebc91590a929bb6 |

